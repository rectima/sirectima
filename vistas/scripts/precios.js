var tabla;

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	
	$("#MODELO_VEHI").prop( "disabled", false );

		$.post("../ajax/precios.php?op=MARCA_PROD", function(r){
			$("#MARCA_PROD").html(r);
			$('#MARCA_PROD').selectpicker('refresh');
		});

		/*$.post("../ajax/precios.php?op=MARCA_VEHI", function(r){
			$("#MARCA_VEHI").html(r);
			$('#MARCA_VEHI').selectpicker('refresh');
		});
*/
		/*$.post("../ajax/precios.php?op=MODELO_VEHI", function(r){
			$("#MODELO_VEHI").html(r);
			$('#MODELO_VEHI').selectpicker('refresh');
		});*/

		$.post("../ajax/precios.php?op=FAMILIA", function(r){
			$("#FAMILIA").html(r);
			$('#FAMILIA').selectpicker('refresh');
		});
               /* $.post("../ajax/precios.php?op=LINEA", function(r){
			$("#LINEA").html(r);
			$('#LINEA').selectpicker('refresh');
		});*/
                  /*$.post("../ajax/precios.php?op=SUBLINEA", function(r){
			$("#SUBLINEA").html(r);
			$('#SUBLINEA').selectpicker('refresh');
		});*/
                 
		//listar();
}

function cargar(){

	MARCA_VEHI=$('#MARCA_VEHI').val();
        MARCA_PROD=$('#MARCA_PROD').val();
	if(MARCA_VEHI.length!=0){
		$("#MODELO_VEHI").prop( "disabled", false );
		var MARCA_VEHI=$('#MARCA_VEHI').val();
                var MARCA_PROD=$('#MARCA_PROD').val();
		$.post("../ajax/precios.php?op=MODELO_VEHI_MARCA&MARCA_VEHI="+MARCA_VEHI+"&MARCA_PROD="+MARCA_PROD, function(r){
			$("#MODELO_VEHI").html(r);
			$('#MODELO_VEHI').selectpicker('refresh');
		});
	}else{
		$("#MODELO_VEHI").prop( "disabled", true );
	}

	

}

function descargar()
{
    
 
//        
        var MARCA_PROD=$('#MARCA_PROD').val();
	var MARCA_VEHI=$('#MARCA_VEHI').val();
	var MODELO_VEHI=$('#MODELO_VEHI').val();
	var FAMILIA=$('#FAMILIA').val();
	var ruc=$('#ruc').val();
	var vendedor=$('#vendedor').val();
	var linea=$('#LINEA').val();
        var sublinea=$('#SUBLINEA').val();
       
         

	if(vendedor==="" || ruc===""){
		bootbox.alert("ERROR DE USUARIO O CLIENTE");
	}else{
	

	$('body').loading();
    $.ajax({
        url:'../ajax/precios.php?op=descargar',
        type: 'get',
        data: { MARCA_PROD:MARCA_PROD ,MARCA_VEHI:MARCA_VEHI, MODELO_VEHI:MODELO_VEHI ,
            FAMILIA:FAMILIA,ruc:ruc,vendedor:vendedor,linea:linea,sublinea:sublinea  },
        success:function(data){
            $('body').loading('stop');
		
				
				 window.open('../files/'+data,'_blank');
				reset();
		

        }
    }).fail(function( jqXHR, textStatus, errorThrown ) {
        $('body').loading('stop');
         if ( console && console.log ) {
            bootbox.alert('Error!', errorThrown+', Los datos que ingresa son incorrectos!', 'error');
         }
    });
	
	}
        
}
function cargarlinea(){

	FAMILIA=$('#FAMILIA').val();
       
	if(FAMILIA.length!=0){
		$("#LINEA").prop( "disabled", false );
		var FAMILIA=$('#FAMILIA').val();
               
		$.post("../ajax/precios.php?op=BUSCA_LINEA&FAMILIA="+FAMILIA, function(r){
			$("#LINEA").html(r);
			$('#LINEA').selectpicker('refresh');
		});
	}else{
		$("#LINEA").prop( "disabled", true );
	}

	

}
function cargarmarca(){

	MARCA_PROD=$('#MARCA_PROD').val();
	if(MARCA_PROD.length!=0){
		$("#MARCA_VEHI").prop( "disabled", false );
		var MARCA_PROD=$('#MARCA_PROD').val();
		$.post("../ajax/precios.php?op=BUSCA_MARCA_VEHICULO&MARCA_PROD="+MARCA_PROD, function(r){
			$("#MARCA_VEHI").html(r);
			$('#MARCA_VEHI').selectpicker('refresh');
		});
	}else{
		$("#MARCA_VEHI").prop( "disabled", true );
	}

	

}
function cargarsub(){

	LINEA=$('#LINEA').val();
        FAMILIA=$('#FAMILIA').val();
        console.log(LINEA.length);
	if(LINEA.length!=0){
		$("#SUBLINEA").prop( "disabled", false );
		var LINEA=$('#LINEA').val();
                var FAMILIA=$('#FAMILIA').val();
		$.post("../ajax/precios.php?op=BUSCASUBLINEA&LINEA="+LINEA+"&FAMILIA="+FAMILIA, function(r){
			$("#SUBLINEA").html(r);
			$('#SUBLINEA').selectpicker('refresh');
		});
	}else{
		$("#SUBLINEA").prop( "disabled", true );
	}

	

}


function reset(){
	$("#MARCA_PROD").val([]);
	$('#MARCA_PROD').selectpicker('refresh');

	$("#MARCA_VEHI").val([]);
	$('#MARCA_VEHI').selectpicker('refresh');

	$.post("../ajax/precios.php?op=MODELO_VEHI", function(r){
		$("#MODELO_VEHI").html(r);
		$('#MODELO_VEHI').selectpicker('refresh');
	});

	$("#FAMILIA").val("");
	$('#FAMILIA').selectpicker('refresh');
        
        $("#SUBLINEA").val("");
	$('#SUBLINEA').selectpicker('refresh');

	$("#linea").val("");
	$('#linea').selectpicker('refresh');

	$("#MODELO_VEHI").prop( "disabled", false );
}

//Función limpiar
function limpiar()
{
	$("#idcategoria").val("");
	$("#nombre").val("");
	$("#descripcion").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{

	var MARCA_PROD=$('#MARCA_PROD').val();
	var MARCA_VEHI=$('#MARCA_VEHI').val();
	var MODELO_VEHI=$('#MODELO_VEHI').val();
	var FAMILIA=$('#FAMILIA').val();
	var ruc=$('#ruc').val();
	var vendedor=$('#vendedor').val();
	var linea=$('#LINEA').val();
        var sublinea=$('#SUBLINEA').val();
       
         

	if(vendedor==="" || ruc===""){
		bootbox.alert("ERROR DE USUARIO O CLIENTE");
	}else{
	

	$('body').loading();
    $.ajax({
        url:'../ajax/precios.php?op=listar',
        type: 'get',
        data: { MARCA_PROD:MARCA_PROD ,MARCA_VEHI:MARCA_VEHI, MODELO_VEHI:MODELO_VEHI ,
            FAMILIA:FAMILIA,ruc:ruc,vendedor:vendedor,linea:linea,sublinea:sublinea  },
        success:function(data){
            $('body').loading('stop');
		 console.log(data);

		 if(data){
				var resp=data.split("___");
				console.log(resp[1]);
				bootbox.alert("SE GENERO Y SE ENVIO EL REPORTE ");
				reset();
			}

        }
    }).fail(function( jqXHR, textStatus, errorThrown ) {
        $('body').loading('stop');
         if ( console && console.log ) {
            bootbox.alert('Error!', errorThrown+', Los datos que ingresa son incorrectos!', 'error');
         }
    });
	
	}
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/categoria.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});

	limpiar();
}

function mostrar(idcategoria)
{
	$.post("../ajax/categoria.php?op=mostrar",{idcategoria : idcategoria}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#descripcion").val(data.descripcion);
 		$("#idcategoria").val(data.idcategoria);

 	})
}

//Función para desactivar registros
function desactivar(idcategoria)
{
	bootbox.confirm("¿Está Seguro de desactivar la Categoría?", function(result){
		if(result)
        {
        	$.post("../ajax/categoria.php?op=desactivar", {idcategoria : idcategoria}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idcategoria)
{
	bootbox.confirm("¿Está Seguro de activar la Categoría?", function(result){
		if(result)
        {
        	$.post("../ajax/categoria.php?op=activar", {idcategoria : idcategoria}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();