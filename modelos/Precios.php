<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/ConexionSQL.php";

Class Precios
{
	//Implementamos nuestro constructor
	public function __construct()
	{

    }
    
    public function MARCA_PROD(){

        $sql="SELECT distinct RTRIM(LTRIM(MARCA)) MARCA from VI_ROTACION where MARCA <> ''";
        return ejecutarConsultaSQL($sql);

    }

    public function MARCA_VEHI(){

        $sql="SELECT distinct RTRIM(LTRIM(MARCA_VEHI)) MARCA_VEHI from VI_ROTACION where MARCA_VEHI <> ''";
        return ejecutarConsultaSQL($sql);

    }

    public function MODELO_VEHI(){

        $sql="SELECT distinct RTRIM(LTRIM(MODELO_VEHI)) MODELO_VEHI from VI_LISTAPRECIOS where MODELO_VEHI <> ''";
        return ejecutarConsultaSQL($sql);

    }

    public function FAMILIA(){

        $sql="SELECT DISTINCT LTRIM(RTRIM(FAMILIA)) Linea_desc  from VI_ROTACION WHERE FAMILIA <> ''";
        return ejecutarConsultaSQL($sql);

    }
    public function LINEA(){

        $sql="SELECT DISTINCT LTRIM(RTRIM(LINEA_COMERCIAL)) LINEA_COMERCIAL  from VI_ROTACION WHERE LINEA_COMERCIAL <> ''";
        return ejecutarConsultaSQL($sql);

    }
    public function SUBLINEA(){
       
        
        $sql="SELECT DISTINCT LTRIM(RTRIM(SUBLINEA)) SUBLINEA  from VI_ROTACION WHERE SUBLINEA <>'' ";
        return ejecutarConsultaSQL($sql);

    }

    public function selectModeloMarca($MARCA_VEHI,$MARCA_PROD){

        $MARCA_VEHI=str_replace(" ","%",$MARCA_VEHI);
        $MARCA_VEHI=str_replace("%20","%",$MARCA_VEHI);
        
        $MARCA_PROD=str_replace(" ","%",$MARCA_PROD);
        $MARCA_PROD=str_replace("%20","%",$MARCA_PROD);
        
        $sql="SELECT distinct RTRIM(LTRIM(MODELO_VEHI)) MODELO_VEHI from VI_ROTACION where MARCA_VEHI <> '' and MARCA_VEHI like '%$MARCA_VEHI%' and MARCA like '%$MARCA_PROD%'";
     
        return ejecutarConsultaSQL($sql);

    }
    public function selectMarcavehiculo($MARCA_PROD){

        $MARCA_PROD=str_replace(" ","%",$MARCA_PROD);
        $MARCA_PROD=str_replace("%20","%",$MARCA_PROD);
        
        $sql="SELECT distinct RTRIM(LTRIM(MARCA_VEHI)) MARCA_VEHI from VI_ROTACION where MARCA <> '' and MARCA like '%$MARCA_PROD%'";
     
        return ejecutarConsultaSQL($sql);

    }
    public function selectSublinea($LINEA,$FAMILIA){

        $LINEA=str_replace(" ","%",$LINEA);
        $LINEA=str_replace("%20","%",$LINEA);
        $FAMILIA=str_replace(" ","%",$FAMILIA);
        $FAMILIA=str_replace("%20","%",$FAMILIA);
        
        $sql="SELECT distinct RTRIM(LTRIM(SUBLINEA)) SUBLINEA from VI_ROTACION where LINEA_COMERCIAL <> ''  and LINEA_COMERCIAL like '%$LINEA%' and familia like'%$FAMILIA%'";
     
        return ejecutarConsultaSQL($sql);

    }
     public function selectLinea($FAMILIA){

        $FAMILIA=str_replace(" ","%",$FAMILIA);
        $FAMILIA=str_replace("%20","%",$FAMILIA);
        
        $sql="SELECT distinct RTRIM(LTRIM(LINEA_COMERCIAL)) LINEA_COMERCIAL from VI_ROTACION where FAMILIA <> ''  and FAMILIA like '%$FAMILIA%'";
     
        return ejecutarConsultaSQL($sql);

    }

    public function listar($MARCA_PROD,$MARCA_VEHI,$MODELO_VEHI,$FAMILIA,$RUC,$VENDEDOR,$GENERAL,$SUBLINEA){

  
        $MODELO_VEHI=str_replace(" ","%",$MODELO_VEHI);
        $MODELO_VEHI=str_replace("%20","%",$MODELO_VEHI);
        $FAMILIA=str_replace(" ","%",$FAMILIA);
        $FAMILIA=str_replace("%20","%",$FAMILIA);

        $con=0;
        $like_MARCA_PROD='(';
       
      

        if($MARCA_PROD==""  && $MARCA_VEHI==""  &&  $MODELO_VEHI==""  &&  $FAMILIA=="" ){
            //consula general

            $sql=" SELECT top 1 tipo FROM [GA_VTA_CTR_LISTA_PRECIOS] WHERE tipo='GENERAL' AND ruc='$RUC' AND vendedor='$VENDEDOR' AND MONTH(fecha)= MONTH(GETDATE()) ";
          
            $resp=ejecutarConsultaSQL($sql);

            if(empty($resp->fetchObject())){

                $sql="INSERT INTO [GA_VTA_CTR_LISTA_PRECIOS] (tipo,ruc,fecha,vendedor,filtros) VALUES ('GENERAL','$RUC',GETDATE(),'$VENDEDOR','$GENERAL')";
            
                ejecutarConsultaSQL($sql);

                if($GENERAL=='CARROCERIA'){

                    $sql="SELECT top 1 CODIGO_EMPRESA,
                    DESCRIPCION,
                    MARCA_PROD,
                    MARCA_VEHI,
                    MODELO_VEHI,
                    FAMILIA,
                    PRECIO_MAYORISTA,
                    PRECIO_MAYORISTA_IVA,
                    PRECIO_MINORISTA,
                    PRECIO_MINORISTA_IVA,
                    PRECIO_ESPECIAL,
                    CANT_MATRIZ,
                    PRECIO_ESPECIAL_IVA FROM VI_ROTACION WHERE RTRIM(LTRIM(SUBLINEA)) ='COLISIÓN - CARROCERIA' and CANT_MATRIZ >0";

                }else{
                    $sql="SELECT top 1 CODIGO_EMPRESA,
                    DESCRIPCION,
                    MARCA_PROD,
                    MARCA_VEHI,
                    MODELO_VEHI,
                    FAMILIA,
                    PRECIO_MAYORISTA,
                    PRECIO_MAYORISTA_IVA,
                    PRECIO_MINORISTA,
                    PRECIO_MINORISTA_IVA,
                    PRECIO_ESPECIAL,
                    CANT_MATRIZ,
                    PRECIO_ESPECIAL_IVA FROM VI_ROTACION WHERE RTRIM(LTRIM(SUBLINEA)) IN('MOTOR - MOTOR DIESEL','MOTOR - MOTOR GASOLINA') and CANT_MATRIZ >0";
                        
                }

               
                return ejecutarConsultaSQL($sql);
                
            }else{

                return 'GENERAL';

            }

        }else{

            $sql=" SELECT COUNT(ID) CONTADOR  FROM [GA_VTA_CTR_LISTA_PRECIOS] WHERE tipo='ESPECIALISTA' AND ruc='$RUC' AND vendedor='$VENDEDOR' AND MONTH(fecha)= MONTH(GETDATE())  ";
        
            $resp=ejecutarConsultaSQL($sql)->fetchObject();

          
            if((int)$resp->CONTADOR<2){

                $filtro=$like_MARCA_PROD.'_'.$like_MARCA_VEHI.'_'.$MODELO_VEHI.'_'.$FAMILIA;
                $filtro=str_replace("%"," ",$filtro);
                $filtro=str_replace("'","",$filtro);
                $filtro=str_replace("(","",$filtro);
                $filtro=str_replace(")","",$filtro);

                $sql="INSERT INTO [GA_VTA_CTR_LISTA_PRECIOS] (tipo,ruc,fecha,vendedor,filtros) VALUES ('ESPECIALISTA','$RUC',GETDATE(),'$VENDEDOR','$filtro')";
               
                ejecutarConsultaSQL($sql);

                $sql="SELECT top 1  CODIGO_EMPRESA,
                DESCRIPCION,
                MARCA_PROD,
                MARCA_VEHI,
                MODELO_VEHI,
                FAMILIA,
                PRECIO_MAYORISTA,
                PRECIO_MAYORISTA_IVA,
                PRECIO_MINORISTA,
                PRECIO_MINORISTA_IVA,
                PRECIO_ESPECIAL,
                CANT_MATRIZ,
                PRECIO_ESPECIAL_IVA FROM VI_ROTACION where $like_MARCA_PROD and $like_MARCA_VEHI  and MODELO_VEHI like '%$MODELO_VEHI%' and ltrim(rtrim(SUBLINEA)) like '%$FAMILIA%' and CANT_MATRIZ >0";
               
                return ejecutarConsultaSQL($sql);
                
            }else{

                return 'ESPECIALISTA';

            }

        }
    }

    public function listar2($MARCA_PROD,$MARCA_VEHI,$MODELO_VEHI,$FAMILIA,$RUC,$VENDEDOR,$LINEA,$SUBLINEA){

        
      
        $MODELO_VEHI=str_replace(" ","%",$MODELO_VEHI);
        $MODELO_VEHI=str_replace("%20","%",$MODELO_VEHI);
        $FAMILIA=str_replace(" ","%",$FAMILIA);
        $FAMILIA=str_replace("%20","%",$FAMILIA);
        $MARCA_PROD=str_replace(" ","%",$MARCA_PROD);
        $MARCA_PROD=str_replace("%20","%",$MARCA_PROD);
        $MARCA_VEHI=str_replace(" ","%",$MARCA_VEHI);
        $MARCA_VEHI=str_replace("%20","%",$MARCA_VEHI);
        $SUBLINEA=str_replace(" ","%",$SUBLINEA);
        $SUBLINEA=str_replace("%20","%",$SUBLINEA);
        $LINEA=str_replace(" ","%",$LINEA);
        $LINEA=str_replace("%20","%",$LINEA);
        $RUC=str_replace(" ","%",$RUC);
        $RUC=str_replace("%20","%",$RUC);
        
       
        $nivelprecio=$this->nivelprecio($RUC)->fetchObject();
      

        if($MARCA_PROD==""  && $MARCA_VEHI==""  &&  $MODELO_VEHI==""  &&  $FAMILIA=="" && $LINEA=="" && $SUBLINEA==""){
            //consula general
           
          
           
                $sql="select a.codigo_empresa,a.descripcion,a.marca,a.modelo_vehi,a.qty_matriz,a.linea_comercial,a.sublinea,a.marca_vehi,a.familia,isnull(ROUND(((b.UOMPRICE) ), 2), 0) precio,a.qty_matriz cantidad from VI_ROTACION a "
                   ." inner join iv00108 b on a.codigo_empresa=b.itemnmbr "
                   ." where b.prclevel='$nivelprecio->prclevel' and a.qty_matriz >0 " ;

        
                return ejecutarConsultaSQL($sql);
                
            }

        else{
                
                $sql="select a.codigo_empresa,a.descripcion,a.marca,a.modelo_vehi,a.qty_matriz,a.linea_comercial,a.sublinea,a.marca_vehi,a.familia,isnull(ROUND(((b.UOMPRICE) ), 2), 0) precio,a.qty_matriz cantidad from VI_ROTACION a
                    inner join iv00108 b on a.codigo_empresa=b.itemnmbr
                    where b.prclevel='$nivelprecio->prclevel' and a.marca like '%$MARCA_PROD%' and a.linea_comercial like'%$LINEA%'
                    and a.sublinea like '%$SUBLINEA%' AND a.modelo_vehi like '%$MODELO_VEHI%' AND  a.marca_vehi like '%$MARCA_VEHI%' 
                    and a.familia like '%$FAMILIA%'  and a.qty_matriz >0";
              
              
                return ejecutarConsultaSQL($sql);
                
            } 
    }

    public function Vendedor($vendedor) {
        
        $sql="select slprsnid,slprsnfn NOMBRE,sprsnsmn,sprsnsln APELLIDO from RM00301 where SLPRSNID='$vendedor'";
        return ejecutarConsultaSQL($sql)->fetchObject();
    }
    public function Cliente($ruc) {
        
        $sql="select CUSTNMBR RUC,CUSTNAME NOMBRE,STMTNAME NOMBRE_COMERCIAL from RM00101 where CUSTNMBR='$ruc'";
        return ejecutarConsultaSQL($sql)->fetchObject();
    }
    public function nivelprecio($RUC){
        
        $sql="select prclevel from rm00101 where custnmbr='$RUC'";
        return ejecutarConsultaSQL($sql);
    }

    public function selectEmail($vendedor){
        $sql=" SELECT username, correo FROM GA_Vendedores WHERE username = '$vendedor'";
        return ejecutarConsultaSQL($sql);
    }


    public function reporteEmail($MARCA_PROD,
    $MARCA_VEHI,
    $MODELO_VEHI,
    $FAMILIA,$RUC,$VENDEDOR,$LINEA,$SUBLINEA){

            $vendedor1= $this->Vendedor($VENDEDOR);
            $cliente1= $this->Cliente($RUC);
            
            $rspta=$this->listar2($MARCA_PROD,
            $MARCA_VEHI,
            $MODELO_VEHI,
            $FAMILIA,$RUC,$VENDEDOR,$LINEA,$SUBLINEA);

        
          
                $cabecera='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
                <HTML>
                <HEAD>
                    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
                    <TITLE></TITLE>
                    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
                    <META NAME="AUTHOR" CONTENT="DANNY GARCIA">
                    <META NAME="CREATED" CONTENT="20201014;134700000000000">
                    <META NAME="CHANGEDBY" CONTENT="DANNY GARCIA">
                    <META NAME="CHANGED" CONTENT="20201014;135200000000000">
                    <META NAME="AppVersion" CONTENT="16.0000">
                    <META NAME="DocSecurity" CONTENT="0">
                    <META NAME="HyperlinksChanged" CONTENT="false">
                    <META NAME="LinksUpToDate" CONTENT="false">
                    <META NAME="ScaleCrop" CONTENT="false">
                    <META NAME="ShareDoc" CONTENT="false">
                    <STYLE TYPE="text/css">
                    <!--
                        @page { size: landscape; margin-left: 0.98in; margin-right: 0.98in; margin-top: 1.18in; margin-bottom: 1.18in }
                        P { margin-bottom: 0.08in; direction: ltr; widows: 2; orphans: 2 }
                    -->
                    </STYLE>
                </HEAD>
                <BODY LANG="en-US" DIR="LTR">
                <P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><FONT SIZE=4 STYLE="font-size: 16pt"><I><B>REPORTE
                DE LISTA DE PRECIOS</B></I></FONT></P>
                <TABLE WIDTH=933 CELLPADDING=7 CELLSPACING=0>
                    <COL WIDTH=296>
                    <COL WIDTH=297>
                    <COL WIDTH=296>
                    <TR VALIGN=TOP>
                        <TD WIDTH=296 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                            <P><B>COD VEND:</B> '.$VENDEDOR.'</P>
                        </TD>
                        <TD WIDTH=296 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                            <P><B>NOM VEND:</B> '.$vendedor1->NOMBRE.'</P>
                        </TD>
                         </TR>
                         <TR VALIGN=TOP>
                         <TD WIDTH=296 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                            <P><B>RUC:</B> '.$RUC.'</P>
                        </TD>
                         <TD WIDTH=296 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                            <P><B>NOM CLIENTE:</B> '.$cliente1->NOMBRE.'</P>
                        </TD>
                         </TR>
                </TABLE>
                <P STYLE="margin-bottom: 0.11in"><BR><BR>
                </P>
                        <TABLE WIDTH=933 CELLPADDING=7 CELLSPACING=0>
                <COL WIDTH=102>
                <COL WIDTH=102>
                <COL WIDTH=103>
                <COL WIDTH=103>
                <COL WIDTH=103>
                <COL WIDTH=103>
                <COL WIDTH=103>
                <COL WIDTH=102>
                <TR VALIGN=TOP>
                    <TD WIDTH=102 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>CODIGO</B></P>
                    </TD>
                    <TD WIDTH=302 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>DESCRIPCION</B></P>
                    </TD>
                    <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>MARCA_PROD</B></P>
                    </TD>
                   
                   <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>CANTIDAD</B></P>
                    </TD>
                    <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>PRECIO</B></P>
                    </TD>
                </TR>';

                $detalle='';

                while($reg=$rspta->fetchObject()){

                   
                $cantidad=number_format($reg->cantidad,2,'.','');
                if((int)$cantidad>10){
                    $cantidad="+10";
                }else{
                    $cantidad=(int)$cantidad;
                }
                    $detalle.='<TR VALIGN=TOP>
                    <TD WIDTH=102 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.$reg->codigo_empresa.'</SPAN></FONT></FONT></FONT></P>
                    </TD>
                    <TD WIDTH=302 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.$reg->descripcion.'
                        </SPAN></FONT></FONT></FONT></P>
                    </TD>
                    <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.$reg->marca.'</SPAN></FONT></FONT></FONT></P>
                    </TD>
                     <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.$cantidad.'</SPAN></FONT></FONT></FONT></P>
                    </TD>
                   
                    <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.number_format($reg->precio,2,'.','').'</SPAN></FONT></FONT></FONT></P>
                    </TD>
                </TR>';
                }
            
                $html=$cabecera.$detalle.'
                </TABLE>
                <P STYLE="margin-bottom: 0.11in"><BR><BR>
                </P>
                </BODY>
                </HTML>';
                 require_once '../vendor/PHPMailer-5.2/PHPMailerAutoload.php';
                
               
                 
                //Creamos la instancia de la clase PHPMailer y configuramos la cuenta
                $mail = new PHPMailer();
                 $mail->IsSMTP();
                 $mail->setFrom('info-noreplay@iav.com.ec', 'RECTIMA INDUSTRY');
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = 'tls'; //seguridad
                $mail->Host = "outlook-latam4.office365.com"; // servidor smtp
                $mail->Port = 587; //puerto
                //$mail->Port = 587; //puerto
                 $mail->Username='info-noreplay@iav.com.ec';
                $mail->Password='IAVin2016@';
                //$mail->From='info-noreplay@iav.com.ec';
                //$mail->FromName='LISTADO DE PRECIOS';
                //$mail->Timeout=60;
                $mail->IsHTML(true);
                //Enviamos el correo
                //$mail->AddAddress($correo); //Puede ser Hotmail

                $SelectCorreo=$this->selectEmail($VENDEDOR);

                

                while($regCorreos=$SelectCorreo->fetchObject()){

                    $to_array = explode(';', $regCorreos->correo);
                    foreach($to_array as $address)
                    {
                       $mail->addAddress($address);
                        //$mail->addAddress('jaranda@iav.com.ec');
                    }

                }
                require_once '../vendor/autoload.php';
                //generar pdf
                $date=date('Y-m-d');
                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML($html);
               
                $mpdf->Output('../files/'.$RUC.$date.'.pdf');
                $docpdf='../files/'.$RUC.$date.'.pdf';

                //$mail->AddAddress('dannyggg23@gmail.com');
                $mail->AddAddress('jaranda@iav.com.ec');
                $mail->AddAddress('aoniate@rectima.com.ec');

                $mail->Subject ='LISTADO DE PRECIOS';

                //$mail->Body=$html;
                $mail->Body="<h2>LISTA DE PRECIOS </h2> <BR> 
                <br> <h3>CLIENTE : <STRONG> ".$RUC." </STRONG></h3>
                <br> <h3>VENDEDOR : <STRONG> ".$VENDEDOR." </STRONG></h3>
                ";
                $mail->addAttachment($docpdf, $name = $RUC.$date.".pdf",  $encoding = 'base64', $type = 'application/pdf');
                $mail->AltBody='Se ha generado el listado de precios';
                $exito = $mail->Send();

                return $exito."___ok";

    }
public function descargarpdf($MARCA_PROD,
    $MARCA_VEHI,
    $MODELO_VEHI,
    $FAMILIA,$RUC,$VENDEDOR,$LINEA,$SUBLINEA) {
    
    $vendedor1= $this->Vendedor($VENDEDOR);
    $cliente1= $this->Cliente($RUC);
     
       $rspta=$this->listar2($MARCA_PROD,
            $MARCA_VEHI,
            $MODELO_VEHI,
            $FAMILIA,$RUC,$VENDEDOR,$LINEA,$SUBLINEA);

        $cabecera='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
                <HTML>
                <HEAD>
                    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
                    <TITLE></TITLE>
                    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
                    <META NAME="AUTHOR" CONTENT="DANNY GARCIA">
                    <META NAME="CREATED" CONTENT="20201014;134700000000000">
                    <META NAME="CHANGEDBY" CONTENT="DANNY GARCIA">
                    <META NAME="CHANGED" CONTENT="20201014;135200000000000">
                    <META NAME="AppVersion" CONTENT="16.0000">
                    <META NAME="DocSecurity" CONTENT="0">
                    <META NAME="HyperlinksChanged" CONTENT="false">
                    <META NAME="LinksUpToDate" CONTENT="false">
                    <META NAME="ScaleCrop" CONTENT="false">
                    <META NAME="ShareDoc" CONTENT="false">
                    <STYLE TYPE="text/css">
                    <!--
                        @page { size: landscape; margin-left: 0.98in; margin-right: 0.98in; margin-top: 1.18in; margin-bottom: 1.18in }
                        P { margin-bottom: 0.08in; direction: ltr; widows: 2; orphans: 2 }
                    -->
                    </STYLE>
                </HEAD>
                <BODY LANG="en-US" DIR="LTR">
                <P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><FONT SIZE=4 STYLE="font-size: 16pt"><I><B>REPORTE
                DE LISTA DE PRECIOS</B></I></FONT></P>
                <TABLE WIDTH=933 CELLPADDING=7 CELLSPACING=0>
                    <COL WIDTH=296>
                    <COL WIDTH=297>
                    <COL WIDTH=296>
                    <TR VALIGN=TOP>
                        <TD WIDTH=296 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                            <P><B>COD VEND:</B> '.$VENDEDOR.'</P>
                        </TD>
                        <TD WIDTH=296 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                            <P><B>NOM VEND:</B> '.$vendedor1->NOMBRE.'</P>
                        </TD>
                         </TR>
                         <TR VALIGN=TOP>
                         <TD WIDTH=296 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                            <P><B>RUC:</B> '.$RUC.'</P>
                        </TD>
                         <TD WIDTH=296 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                            <P><B>NOM CLIENTE:</B> '.$cliente1->NOMBRE.'</P>
                        </TD>
                         </TR>
                   
                </TABLE>
                <P STYLE="margin-bottom: 0.11in"><BR><BR>
                </P>
                        <TABLE WIDTH=933 CELLPADDING=7 CELLSPACING=0>
                <COL WIDTH=102>
                <COL WIDTH=102>
                <COL WIDTH=103>
                <COL WIDTH=103>
                <COL WIDTH=103>
                <COL WIDTH=103>
                <COL WIDTH=103>
                <COL WIDTH=102>
                <TR VALIGN=TOP>
                    <TD WIDTH=102 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>CODIGO</B></P>
                    </TD>
                    <TD WIDTH=302 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>DESCRIPCION</B></P>
                    </TD>
                    <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>MARCA_PROD</B></P>
                    </TD>
                   
                   <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>CANTIDAD</B></P>
                    </TD>
                    
                    <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><B>PRECIO</B></P>
                    </TD>
                </TR>';

                $detalle='';

                while($reg=$rspta->fetchObject()){

                   
                $cantidad=number_format($reg->cantidad,2,'.','');
                if((int)$cantidad>10){
                    $cantidad="+10";
                }else{
                    $cantidad=(int)$cantidad;
                }
                    $detalle.='<TR VALIGN=TOP>
                    <TD WIDTH=102 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.$reg->codigo_empresa.'</SPAN></FONT></FONT></FONT></P>
                    </TD>
                    <TD WIDTH=302 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.$reg->descripcion.'
                        </SPAN></FONT></FONT></FONT></P>
                    </TD>
                    <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.$reg->marca.'</SPAN></FONT></FONT></FONT></P>
                    </TD>
                   <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.$cantidad.'</SPAN></FONT></FONT></FONT></P>
                    </TD>
                    <TD WIDTH=103 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                        <P ALIGN=CENTER><FONT COLOR="#333333"><FONT FACE="Source Sans Pro, serif"><FONT SIZE=2 STYLE="font-size: 10pt"><SPAN STYLE="background: #f9f9f9">'.number_format($reg->precio,2,'.','').'</SPAN></FONT></FONT></FONT></P>
                    </TD>
                </TR>';
                }
            
                $html=$cabecera.$detalle.'
                </TABLE>
                <P STYLE="margin-bottom: 0.11in"><BR><BR>
                </P>
                </BODY>
                </HTML>';
     require_once '../vendor/autoload.php';
                //generar pdf
     
                $date=date('Y-m-d H:i:s');
                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML($html);
               //$mpdf->Output();
               $mpdf->Output('../files/'.$RUC.$date.'.pdf');
               $docpdf='../files/'.$RUC.$date.'.pdf';
              
               return  $docpdf;
               
}











}
