<?php

 require "../config/ConexionSQL.php";

Class Clientes
 {
  public function _construct(){
  }
    
      public function get_clientes()
    {
        $sql="select rtrim(a.custnmbr) as RUC,rtrim(a.custname) as Cliente,rtrim(STMTNAME) as RazonSocial, rtrim(a.SLPRSNID) as Vendedor,rtrim(a.PRCLEVEL) as NivelPrecio,rtrim(a.ADDRESS1) as Dirrecion,rtrim(a.PHONE1) as Telefono,rtrim(a.CITY) as Ciudad,rtrim(a.STATE) as Provincia,rtrim(a.country) as Pais
            ,(CASE CRLMTTYP WHEN 1 THEN 200000 ELSE(SELECT CRLMTAMT FROM RM00101 X WITH(NOLOCK) WHERE X.CUSTNMBR = a.CUSTNMBR)END) as Cupo
            ,(select top 1 rtrim(z.data) from ALT_MIS_INFOADD z WITH(NOLOCK) where z.MASTERID = a.custnmbr and FIELD = 'Email') as Correo
            ,rtrim(a.SHRTNAME) as idCorto,rtrim(a.cntcprsn) as Contacto,rtrim(a.PYMTRMID) as CondicionPago,
            SALSTERR as 'idterritorio',CCode as 'CodigoPais',PRCLEVEL as 'IddeClase',SHIPMTHD as 'MetododeEnvio',TAXSCHID as 'IddeplandeImpuestos'
             ,HOLD as 'Suspencion',INACTIVE as 'Inactivo',0 as 'ListaNegra',0 as 'UAF', (select case when count(cb.ruc) > 0 then 1 else 0 end as Bajo from GR_CLIENTES_BAJOMONTO cb WITH(NOLOCK) where cb.ruc = a.custnmbr) as BajoMonto
            ,case when(select count(gcli.ruc) from GA_CLI_Trelacionados gcli where gcli.ruc = a.CUSTNMBR) > 0 then 'SI' else 'NO' end as relacionado
             ,cast(cast(CREATDDT as date) as nvarchar(20)) as fechaCreacion
            from rm00101 a WITH(NOLOCK) where isnumeric(CUSTNMBR) = 1";
        
        return ejecutarConsultaSQL($sql);
    }
    
 }
?>
